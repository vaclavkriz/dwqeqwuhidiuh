const {writeFile} = require('fs/promises')

let tickets = [];

for (let index = 0; index < 10000; index++) {
    tickets.push({
        totalPay: 123,
        selections: 
            Array(80).fill({
                a: 1,
                b: 2,
                c: true,
                d: "xxx",
                f: 3213,
                e: false
            })
    
    })
}

(async () => writeFile("./tickets-1000.json", JSON.stringify(tickets)))()