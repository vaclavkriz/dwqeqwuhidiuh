export interface CreateTicketRequest {
    bets: Bet[]
    e_ticket: boolean
    ip_confirmed: boolean
    items: Item[]
    notify: boolean
    use_bonus: boolean
    verify: Verify
  }
  
  export interface Bet {
    bet_value: string
    combination: number
  }
  
  export interface Item {
    group: string
    id_odds: string
    odds_value: string
  }
  
  export interface Verify {
    total_stake: string
    total_win_to_pay: string
  }
  