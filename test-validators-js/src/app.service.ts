import { Injectable } from '@nestjs/common';

function validateIntValues(a: number, b: number) {
  if (a === b) {
  }
}

function validateOddsValues(a: number, b: number) {
  if (a < b) {
  }
}

@Injectable()
export class AppService {
  getHello(param: number): any {
    const tickets = [];

    for (let index = 0; index < param; index++) {
      const selections = [];
      for (let j = 0; j < 80; j++) {
        selections.push({
          a: 1,
          b: 2,
          c: true,
          d: 'xxx',
          f: 3213,
          e: false,
          oddsValue: Math.floor(Math.random() * 100),
        });
      }

      tickets.push({
        oddsValue: 321312,
        selections,
      })
    }

    for (const ticket of tickets) {
      let totalOdds = 0;

      for (const [i, selection1] of ticket.selections.entries()) {
        for (const [j, selection2] of ticket.selections.entries()) {
          if (i !== j) {
            totalOdds *= selection1.oddsValue * selection2.oddsValue;
            validateIntValues(selection1.a, selection2.a);
            validateIntValues(selection1.b, selection2.b);
            validateIntValues(selection1.f, selection2.f);
          }
        }
      }
      validateOddsValues(ticket.totalOdds, totalOdds);
    }
  }
}
