import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DATA_RETRIEVER, ExpressDefaultDataRetrieverService, LoggerModule } from '@betsys-nestjs/logger';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    LoggerModule.forRoot("express", [
    {
      provide: DATA_RETRIEVER,
      useClass: ExpressDefaultDataRetrieverService
    }
  ]),
  ConfigModule.forRoot()
],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
