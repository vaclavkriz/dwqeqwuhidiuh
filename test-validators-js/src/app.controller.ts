import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/:tickets')
  getHello(@Param('tickets') param: number): any {
    return this.appService.getHello(param);
  }
}
