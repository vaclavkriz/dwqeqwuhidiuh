package main

import (
	"strconv"

	"github.com/gin-gonic/gin"
)

type Selection struct {
	a         int
	b         int
	c         bool
	d         string
	f         int
	e         bool
	oddsValue float32
}

type Ticket struct {
	totalPay   float32
	selections []Selection
	totalOdds  float32
}

func validateIntValues(a int, b int) {
	if a == b {
		// fmt.Printf("Duplicate 'a' value: %d, 'b' value: %d, found in selections\n", a, b)
	}
}

func validateOddsValues(totalOddsValue float32, selectionOddsValue float32) {
	if totalOddsValue > selectionOddsValue {
		// fmt.Printf("Duplicate 'a' value: %f, 'b' value: %f, found in selections\n", totalOddsValue, selectionOddsValue)
	}
}

func main() {
	router := gin.Default()

	router.GET("/test/:tickets", func(ctx *gin.Context) {
		params, _ := strconv.Atoi(ctx.Param("tickets"))

		var tickets []Ticket = make([]Ticket, params)

		for i := 0; i < params; i++ {
			selections := make([]Selection, 80)

			for j := 0; j < 80; j++ {
				selection := Selection{
					a:         1,
					b:         1,
					c:         true,
					d:         "RandomString",
					f:         1,
					e:         false,
					oddsValue: 1.3,
				}
				selections[j] = selection
			}

			tickets[i] = Ticket{
				totalPay:   1.22,
				selections: selections,
			}
		}

		for _, ticket := range tickets {
			var totalOdds float32 = 0

			for i, selection1 := range ticket.selections {
				for j, selection2 := range ticket.selections {

					if i != j {
						totalOdds *= selection1.oddsValue * selection2.oddsValue
						validateIntValues(selection1.a, selection2.a)
						validateIntValues(selection1.b, selection2.b)
						validateIntValues(selection1.f, selection2.f)
					}
				}
			}
			validateOddsValues(ticket.totalOdds, totalOdds)
		}
	})

	router.Run("localhost:3001")
}
